%{
Matlab applications in physics
Author: Adam Stasiak
Engineering Physics
%}

%the script should be called with peakAnalisis ('File_path')
function [] = peakAnalisis(filePath)

  file = dir(filePath); %file information
  fileSize = file.bytes; %file size
  fd = fopen(filePath, 'r'); %opening of the analyzed file
  [filepath, fileName, ext] = fileparts(filePath); %breaking the file path into its components
  bufferSize = 10e6; %buffer size

  disp('Data analysis in progress. This may take a while. Please wait.') %message about analysis start

  histData = zeros(256, 1); %table of ADC value repetitions, (table index - 1) corresponds to the ADC value

  %loop that reads data from the file and counts the number of repetitions
  while ~feof(fd)
      dataBuff = fread(fd, bufferSize, 'uint8');
      for j = 1:size(dataBuff, 1)
          histData(dataBuff(j)) = histData(dataBuff(j)) + 1;
      end
      clear dataBuff
  end

  fclose(fd); %closing of the analyzed file

  noiseLevel = NoiseLevel(histData); %setting the noise level

  fd = fopen(filePath, 'r'); %opening of the analyzed file

  %loop that counts pulses
  pulses = 0;
  while ~feof(fd)
      dataBuff = fread(fd, bufferSize, 'uint8');
      pulses = pulses + CountPulses(dataBuff, noiseLevel);
      clear dataBuff;
  end

  fclose(fd); %closing of the analyzed file

  %drawing histogram
  bar([0:255],histData);
  xlabel('values ​​from ADC, quantum');
  ylabel('number of occurrences');
  title('Histogram of ADC values');

  saveas(gcf,  [fileName, '_histogram.pdf']); %saving the histogram to a pdf file

  %saving signal information to a file
  fd = fopen([fileName, '_peak_analysis_results.dat'] ,'w');
  fprintf(fd, 'Noise level: %d\n', noiseLevel);
  fprintf(fd, 'Quantity of pulses: %d\n', pulses);
  pulseUncertainty = round(1/sqrt(pulses), 2, 'significant'); %impulse uncertainty as the inverse of the pulse root
  fprintf(fd, 'Pulse uncertainty: %.4f\n', pulseUncertainty);
  fclose(fd);

  disp('Data analysis completed successfully.'); %analysis completion message

  clear; %clearing memory
end
