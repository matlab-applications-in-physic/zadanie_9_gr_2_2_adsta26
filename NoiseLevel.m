%{
Matlab applications in physics
Author: Adam Stasiak
Engineering Physics
%}

%calculation of signal noise level as mode of values of ADC
function noise = NoiseLevel(data)
    if (size(find(data == max(data)), 1) > 1);
        noise = mean(find(data == max(data)) - 1); %average when the number of occurrences of several values ​​is the same	
    else
        noise = find(data == max(data)) - 1; 
    end
end
