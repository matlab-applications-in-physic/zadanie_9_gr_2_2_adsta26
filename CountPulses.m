%{
Matlab applications in physics
Author: Adam Stasiak
Engineering Physics
%}

%pulse counting function
%the function counts positive pulses lasting at least 3ns and subtracts negative pulses lasting at least 3ns from them
%subtraction is performed on the basis of the assumption that during the disruption the number of positive pulses is equal to the number of negative pulses
function pulses = CountPulses(data, noiseLevel)
    data = nonzeros(diff(data - noiseLevel)); %derivative of (data - noise level) with constant values ​​removed
    pulses = 0;
    i = 1;
    while(i < size(data, 1) - 6)
      if (sum(data(i:i+2)) > 2 && sum(data(i+3:i+5)) < 2);
        pulses = pulses + 1;
      elseif (sum(data(i:i+2)) < 2 && sum(data(i+3:i+5)) > 2);
        pulses = pulses - 1;
      end
      i = i + 6;
    end
end
